// SPDX-License-Identifier: GPL-2.0
/*
 * lt6911uxe.c - Lontium 4k60 HDMI-CSI bridge driver
 *
 * Copyright (c) 2020, Lukas Neuner <neur@zhaw.ch>
 * Copyright (c) 2024 MediaTek Inc.
 */

#include "lt6911uxe_regs.h"
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/version.h>
#include <linux/v4l2-dv-timings.h>
#include <media/v4l2-common.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-dv-timings.h>
#include <media/v4l2-event.h>
#include <media/v4l2-fwnode.h>
#include <media/v4l2-subdev.h>

/* v4l2 debug level */
static int debug;
module_param(debug, int, 0644);
MODULE_PARM_DESC(debug, "debug level (0-3)");

/* custom v4l2 controls */
#define V4L2_CID_USER_LT6911UXE_BASE (V4L2_CID_USER_BASE + 0x1090)
#define LT6911UXE_CID_AUDIO_SAMPLING_RATE (V4L2_CID_USER_LT6911UXE_BASE + 1)
#define LT6911UXE_CID_AUDIO_PRESENT (V4L2_CID_USER_LT6911UXE_BASE + 2)

/* v4l2 dv timings */
static struct v4l2_dv_timings default_timing = V4L2_DV_BT_CEA_3840X2160P30;

static const struct v4l2_dv_timings_cap lt6911uxe_timings_cap_4kp30 = {
	.type = V4L2_DV_BT_656_1120,
	/* keep this initialization for compatibility with GCC < 4.4.6 */
	.reserved = { 0 },
	/* Pixel clock from REF_01 p. 20. Min/max height/width are unknown */
	V4L2_INIT_BT_TIMINGS(
		160, 3840, /* min/max width */
		120, 2160, /* min/max height */
		25000000, 297000000, /* min/max pixelclock */
		V4L2_DV_BT_STD_CEA861 | V4L2_DV_BT_STD_DMT | V4L2_DV_BT_STD_CVT,
		V4L2_DV_BT_CAP_PROGRESSIVE | V4L2_DV_BT_CAP_CUSTOM |
		V4L2_DV_BT_CAP_REDUCED_BLANKING)
};

/* Timings for LT6911UXE */
struct lt6911uxe_dv_timings {
	__u32 width;
	__u32 height;
	__u32 interlaced;
	__u32 htot;
	__u32 vtot;
	__u32 fps;
	__u64 pixelclock;
	__u32 standards;
	__u32 flags;
	bool change;
};

struct lt6911uxe_platform_data {
	/* GPIOs */
	int reset_gpio;
};

struct lt6911uxe_state {
	struct i2c_client *i2c_client;
	struct v4l2_subdev sd;
	struct lt6911uxe_platform_data *pdata;
	struct media_pad pad[1];
	struct mutex lock;
	struct v4l2_ctrl_handler ctrl_handler;
	/* controls */
	u8 bank; /* active reg-bank for I2C */
	bool enable_i2c;
	bool signal_present;
	bool is_streaming;
	/* expose audio capabilities */
	struct v4l2_ctrl *audio_sampling_rate_ctrl;
	struct v4l2_ctrl *audio_present_ctrl;
	/* timing / media format */
	struct v4l2_dv_timings timings;
	struct lt6911uxe_dv_timings detected_timings; /* timings detected from phy */
	u32 mbus_fmt_code; /* current media bus format */
};

struct lt6911uxe_colorfmt {
	unsigned int code;
	enum v4l2_colorspace colorspace;
};

static const struct lt6911uxe_colorfmt lt6911uxe_color_fmts[] = {
	{ MEDIA_BUS_FMT_UYVY8_1X16, V4L2_COLORSPACE_SRGB },
};

static const struct v4l2_event lt6911uxe_ev_source_change = {
	.type = V4L2_EVENT_SOURCE_CHANGE,
	.u.src_change.changes = V4L2_EVENT_SRC_CH_RESOLUTION,
};

static inline struct lt6911uxe_state *to_state(struct v4l2_subdev *sd)
{
	return container_of(sd, struct lt6911uxe_state, sd);
}

static bool lt6911uxe_compare_timings(struct lt6911uxe_dv_timings *a,
				  struct lt6911uxe_dv_timings *b)
{
	if (a->width != b->width ||
	    a->height != b->height ||
	    a->htot != b->htot ||
	    a->vtot != b->vtot ||
	    a->fps != b->fps ||
	    a->pixelclock != b->pixelclock) {
	    return false;
	}

	return true;
}

/* ------ I2C --------------------------------------------------------------- */

static void lt6911uxe_reg_bank(struct v4l2_subdev *sd, u8 bank)
{
	struct lt6911uxe_state *state = to_state(sd);
	struct i2c_client *client = state->i2c_client;
	int err;
	struct i2c_msg msg;
	u8 data[2];
	u8 address;

	if (state->bank == bank)
		return;
	dev_dbg(&client->dev, "i2c: change register bank to 0x%02X\n", bank);

	address = 0xFF;
	msg.addr = client->addr;
	msg.buf = data;
	msg.len = 2;
	msg.flags = 0;

	data[0] = address;
	data[1] = bank;

	err = i2c_transfer(client->adapter, &msg, 1);
	if (err != 1) {
		dev_err(&client->dev,
			"%s: switch to bank 0x%x from 0x%x failed\n", __func__,
			bank, client->addr);
		return;
	}
	state->bank = bank;
}

static void lt6911uxe_i2c_wr8(struct v4l2_subdev *sd, u16 reg, u8 val)
{
	struct lt6911uxe_state *state = to_state(sd);
	struct i2c_client *client = state->i2c_client;
	int err;
	struct i2c_msg msg;
	u8 data[2];
	u8 address;

	/* write register bank offset */
	u8 bank = (reg >> 8) & 0xFF;

	lt6911uxe_reg_bank(sd, bank);

	address = reg & 0xFF;
	msg.addr = client->addr;
	msg.buf = data;
	msg.len = 2;
	msg.flags = 0;

	data[0] = address;
	data[1] = val;

	err = i2c_transfer(client->adapter, &msg, 1);

	if (err != 1) {
		dev_err(&client->dev,
			"%s: write register 0x%x from 0x%x failed\n", __func__,
			reg, client->addr);
		return;
	}
	dev_dbg(&client->dev, "i2c: write register: 0x%04X = 0x%02X\n", reg,
		val);
}

static void lt6911uxe_i2c_rd(struct v4l2_subdev *sd, u16 reg, u8 *values, u32 n)
{
	struct lt6911uxe_state *state = to_state(sd);
	struct i2c_client *client = state->i2c_client;
	int err;
	u8 reg_addr[1] = { (u8)(reg & 0xff) };
	u8 bank_addr = (u8)((reg >> 8) & 0xFF);

	struct i2c_msg msgs[] = {
		{
			.addr = client->addr,
			.flags = 0, /* write */
			.len = 1,
			.buf = reg_addr,
		},
		{
			.addr = client->addr,
			.flags = I2C_M_RD, /* read n bytes */
			.len = n,
			.buf = values,
		},
	};

	/* write register bank offset */
	lt6911uxe_reg_bank(sd, bank_addr);

	err = i2c_transfer(client->adapter, msgs, ARRAY_SIZE(msgs));
	if (err != ARRAY_SIZE(msgs)) {
		dev_err(&client->dev,
			"%s: read register 0x%04X from 0x%x failed\n", __func__,
			reg, client->addr);
	}
}

static u8 lt6911uxe_i2c_rd8(struct v4l2_subdev *sd, u16 reg)
{
	u8 val;

	lt6911uxe_i2c_rd(sd, reg, &val, 1);

	dev_dbg(sd->dev, "i2c: read 0x%04X = 0x%02X\n", reg, val);
	return val;
}

/* ------ STATUS / CTRL ----------------------------------------------------- */

static inline bool no_signal(struct v4l2_subdev *sd)
{
	struct lt6911uxe_state *state = to_state(sd);

	return !state->signal_present;
}

static void lt6911uxe_ext_control(struct v4l2_subdev *sd, bool enable)
{
	struct lt6911uxe_state *state = to_state(sd);

	if (state->enable_i2c == enable)
		return;

	state->enable_i2c = enable;
	if (enable) {
		dev_dbg(sd->dev, "%s(): enable external i2c control\n",
			__func__);
		lt6911uxe_i2c_wr8(sd, ENABLE_I2C, 0x01);
	} else {
		dev_dbg(sd->dev, "%s(): disable external i2c control\n",
			__func__);
		lt6911uxe_i2c_wr8(sd, ENABLE_I2C, 0x00);
	}
}

static int lt6911uxe_csi_enable(struct v4l2_subdev *sd, bool enable)
{
	dev_dbg(sd->dev, "%s(): %d\n", __func__, enable);

	if (enable)
		lt6911uxe_i2c_wr8(sd, MIPI_TX_CTRL, 0x01);
	else
		lt6911uxe_i2c_wr8(sd, MIPI_TX_CTRL, 0x00);

	return 0;
}

static int lt6911uxe_get_audio_sampling_rate(struct lt6911uxe_state *state)
{
	u32 audio_fs, idx, fs1, fs2;
	static const int eps = 1500;
	static const int rates_default[] = { 32000, 44100,  48000, 88200,
					     96000, 176400, 192000 };

	fs1 = lt6911uxe_i2c_rd8(&state->sd, Audio_FS_Value1);
	fs2 = lt6911uxe_i2c_rd8(&state->sd, Audio_FS_Value0);
	audio_fs = (fs1 << 8 | fs2) * 1000;
	dev_info(&state->i2c_client->dev, "%s: Audio sample rate %u [Hz]\n",
		 __func__, audio_fs);

	/* audio_fs is an approximation of sample rate - search nearest */
	for (idx = 0; idx < ARRAY_SIZE(rates_default); ++idx) {
		if ((rates_default[idx] - eps < audio_fs) &&
		    (rates_default[idx] + eps > audio_fs))
			return rates_default[idx];
	}
	dev_err(&state->i2c_client->dev,
		"%s: unhandled sampling rate_H %u [Hz]", __func__, audio_fs);

	return 0;
}

/* ------ TIMINGS ----------------------------------------------------------- */

static int lt6911uxe_detect_timings(struct v4l2_subdev *sd,
				    struct lt6911uxe_dv_timings *timings)
{
	struct lt6911uxe_dv_timings new_timings;
	u32 width, height, htot, vtot;
	u32 fps, half_pixel_clk, pixel_clk;
	u8 width1, width0, height1, height0;
	u8 fm2, fm1, fm0;
	u8 h_htotal1, h_htotal0, vtotal1, vtotal0;

	memset(&new_timings, 0, sizeof(new_timings));
	if (no_signal(sd)) {
		v4l2_err(sd, "%s: no valid signal\n", __func__);
		return -ENOLINK;
	}

	fm2 = lt6911uxe_i2c_rd8(sd, half_PixelClock2);
	fm1 = lt6911uxe_i2c_rd8(sd, half_PixelClock1);
	fm0 = lt6911uxe_i2c_rd8(sd, half_PixelClock0);

	half_pixel_clk = fm2 << 16 | fm1 << 8 | fm0;
	v4l2_dbg(1, debug, sd, "%s(): pixelclock: %u\n", __func__,
		 half_pixel_clk * 2 * 1000);

	h_htotal1 = lt6911uxe_i2c_rd8(sd, half_Htotal1);
	h_htotal0 = lt6911uxe_i2c_rd8(sd, half_Htotal0);
	htot = (h_htotal1 << 8 | h_htotal0) * 2;
	v4l2_dbg(1, debug, sd, "%s(): Htotal: %u\n", __func__, htot);

	vtotal1 = lt6911uxe_i2c_rd8(sd, Vtotal1);
	vtotal0 = lt6911uxe_i2c_rd8(sd, Vtotal0);
	vtot = (vtotal1 << 8 | vtotal0);
	v4l2_dbg(1, debug, sd, "%s(): Vtotal: %u\n", __func__, vtot);

	width1 = lt6911uxe_i2c_rd8(sd, half_Hactive1);
	width0 = lt6911uxe_i2c_rd8(sd, half_Hactive0);
	width = (width1 << 8 | width0) * 2;
	v4l2_dbg(1, debug, sd, "%s(): Width: %u\n", __func__, width);

	height1 = lt6911uxe_i2c_rd8(sd, Vactive1);
	height0 = lt6911uxe_i2c_rd8(sd, Vactive0);
	height = height1 << 8 | height0;
	v4l2_dbg(1, debug, sd, "%s(): Height: %u\n", __func__, height);

	fps = DIV_ROUND_CLOSEST((half_pixel_clk * 2 * 1000), (htot * vtot));
	v4l2_dbg(1, debug, sd, "%s(): FPS: %u\n", __func__, fps);

	pixel_clk = htot * vtot * fps;
	v4l2_dbg(1, debug, sd, "%s(): Pixelclock: %u\n", __func__, pixel_clk);

	new_timings.interlaced = V4L2_DV_PROGRESSIVE;
	new_timings.width = width;
	new_timings.height = height;
	new_timings.htot = htot;
	new_timings.vtot = vtot;
	new_timings.fps = fps;
	new_timings.pixelclock = pixel_clk;

	if (!lt6911uxe_compare_timings(&new_timings, timings)) {
		*timings = new_timings;
		timings->change = true;
	} else {
		timings->change = false;
	}

	return 0;
}

/* ------ CORE OPS ---------------------------------------------------------- */

static int lt6911uxe_log_status(struct v4l2_subdev *sd)
{
	struct lt6911uxe_state *state = to_state(sd);

	v4l2_info(sd, "----- Timings -----\n");
	if (!state->detected_timings.width) {
		v4l2_info(sd, "no video detected\n");
	} else {
		v4l2_info(sd, "detected format: %ux%u%s%u.%02u (%ux%u)\n",
			state->detected_timings.width, state->detected_timings.height,
			state->detected_timings.interlaced ? "i" : "p",
			state->detected_timings.fps, 0,
			state->detected_timings.htot, state->detected_timings.vtot);
		v4l2_info(sd, "pixelclock: %llu\n", state->detected_timings.pixelclock);
	}
	v4l2_print_dv_timings(sd->name, "configured format: ", &state->timings,
			      true);

	return 0;
}

static int lt6911uxe_subscribe_event(struct v4l2_subdev *sd, struct v4l2_fh *fh,
				     struct v4l2_event_subscription *sub)
{
	switch (sub->type) {
	case V4L2_EVENT_SOURCE_CHANGE:
		return v4l2_src_change_event_subdev_subscribe(sd, fh, sub);
	default:
		return -EINVAL;
	}
}

/* ------ IRQ --------------------------------------------------------------- */

static void lt6911uxe_hdmi_int_handler(struct lt6911uxe_state *state,
				       bool *handled)
{
	struct device *dev = &state->i2c_client->dev;
	u8 int_event, fm2, fm1, fm0, lanes;
	u32 byte_clock;

	/* Read interrupt event */
	int_event = lt6911uxe_i2c_rd8(&state->sd, INT_VIDEO);
	dev_info(dev, "%s: Interrupt type = 0x%02X\n", __func__, int_event);

	switch (int_event) {
	case INT_VIDEO_DISAPPEAR:
		/* stop MIPI output */
		dev_info(dev, "Video disappear\n");
		lt6911uxe_csi_enable(&state->sd, false);

		if (state->signal_present) {
			state->signal_present = false;
			v4l2_subdev_notify_event(&state->sd,
						 &lt6911uxe_ev_source_change);
			dev_dbg(dev, "event: no signal\n");
			memset(&state->detected_timings, 0,
			       sizeof(state->detected_timings));
			dev_dbg(dev, "%s: clear lt6911uxe detected timings\n", __func__);
		}
		if (handled)
			*handled = true;
		break;

	case INT_VIDEO_READY:
		dev_info(dev, "Video ready\n");

		/* at each HDMI-stable event renew timings */
		state->signal_present = true;
		lt6911uxe_detect_timings(&state->sd, &state->detected_timings);

		/* byte clock / MIPI clock */
		fm2 = lt6911uxe_i2c_rd8(&state->sd, ByteClock2);
		fm1 = lt6911uxe_i2c_rd8(&state->sd, ByteClock1);
		fm0 = lt6911uxe_i2c_rd8(&state->sd, ByteClock0);

		byte_clock = fm2 << 16 | fm1 << 8 | fm0;
		dev_info(dev, "Byte clock: %u kHz\n", byte_clock);
		dev_info(dev, "MIPI clock rate: %u kHz\n", byte_clock * 4);
		dev_info(dev, "MIPI data rate: %u kHz\n", byte_clock * 8);

		/* MIPI */
		lanes = lt6911uxe_i2c_rd8(&state->sd, MIPI_LANES);
		dev_info(dev, "MIPI lanes: %d\n", lanes);

		if (state->is_streaming)
			lt6911uxe_csi_enable(&state->sd, true);

		if (state->detected_timings.change)
			dev_info(dev, "timings change, store new timings");
		else
			dev_info(dev, "no timings change");

		if (handled)
			*handled = true;
		break;
	default:
		dev_info(dev, "%s: unhandled = 0x%02X\n", __func__, int_event);
		return;
	}
}

static void lt6911uxe_audio_int_handler(struct lt6911uxe_state *state,
					bool *handled)
{
	u8 int_event;
	int audio_fs = 0;
	struct device *dev = &state->i2c_client->dev;

	/* read interrupt event */
	int_event = lt6911uxe_i2c_rd8(&state->sd, INT_AUDIO);
	dev_info(dev, "%s: Interrupt type =  0x%02X\n", __func__, int_event);

	switch (int_event) {
	case INT_AUDIO_DISAPPEAR:
		dev_info(dev, "Audio disappear\n");
		audio_fs = 0;
		break;
	// case INT_AUDIO_SR_HIGH:
	case INT_AUDIO_READY:
		dev_info(dev, "Audio ready\n");
		audio_fs = lt6911uxe_get_audio_sampling_rate(state);
		break;
	default:
		dev_info(dev, "%s: unhandled = 0x%02X\n", __func__, int_event);
		return;
	}

	v4l2_ctrl_s_ctrl(state->audio_present_ctrl, (audio_fs != 0));
	v4l2_ctrl_s_ctrl(state->audio_sampling_rate_ctrl, audio_fs);

	if (handled)
		*handled = true;
}

static int lt6911uxe_isr(struct v4l2_subdev *sd, bool *handled)
{
	struct lt6911uxe_state *state = to_state(sd);

	mutex_lock(&state->lock);
	dev_dbg(sd->dev, "%s in kthread %d\n", __func__, current->pid);

	lt6911uxe_ext_control(sd, true);

	/* Retrieve interrupt event */
	lt6911uxe_hdmi_int_handler(state, handled);

	lt6911uxe_audio_int_handler(state, handled);

	lt6911uxe_log_status(sd);

	lt6911uxe_ext_control(sd, false);

	mutex_unlock(&state->lock);
	return 0;
}

static irqreturn_t lt6911uxe_irq_handler(int irq, void *dev_id)
{
	struct v4l2_subdev *sd = dev_id;
	bool handled = false;

	lt6911uxe_isr(sd, &handled);

	return handled ? IRQ_HANDLED : IRQ_NONE;
}

/* ------ VIDEO OPS --------------------------------------------------------- */

static const struct v4l2_dv_timings_cap *
lt6911uxe_g_timings_cap(struct lt6911uxe_state *state)
{
	return &lt6911uxe_timings_cap_4kp30;
}

static int lt6911uxe_g_input_status(struct v4l2_subdev *sd, u32 *status)
{
	*status = 0;
	*status |= no_signal(sd) ? V4L2_IN_ST_NO_SIGNAL : 0;

	v4l2_dbg(1, debug, sd, "%s: status = 0x%x\n", __func__, *status);
	return 0;
}

static int lt6911uxe_s_dv_timings(struct v4l2_subdev *sd,
				  struct v4l2_dv_timings *timings)
{
	struct lt6911uxe_state *state = to_state(sd);

	if (!v4l2_valid_dv_timings(timings, lt6911uxe_g_timings_cap(state),
				   NULL, NULL)) {
		v4l2_err(sd, "%s: timings out of range\n", __func__);
		return -EINVAL;
	}

	v4l2_find_dv_timings_cap(timings, lt6911uxe_g_timings_cap(state), 0,
				 NULL, NULL);

	/* Verify if new timings match current timings */
	if (v4l2_match_dv_timings(timings, &state->timings, 0, false)) {
		v4l2_info(sd, "%s: no change\n", __func__);
		return 0;
	}

	memset(timings->bt.reserved, 0, sizeof(timings->bt.reserved));
	state->timings = *timings;

	if (debug)
		v4l2_print_dv_timings(sd->name, "s_dv_timings: ",
				      &state->timings, true);
	return 0;
}

static int lt6911uxe_g_dv_timings(struct v4l2_subdev *sd,
				  struct v4l2_dv_timings *timings)
{
	struct lt6911uxe_state *state = to_state(sd);

	*timings = state->timings;
	return 0;
}

static int lt6911uxe_query_dv_timings(struct v4l2_subdev *sd,
				      struct v4l2_dv_timings *timings)
{
	struct lt6911uxe_state *state = to_state(sd);

	if (no_signal(sd)) {
		v4l2_warn(sd, "%s: no valid signal\n", __func__);
		return -ENOLINK;
	}

	/* Only query configured timings */
	if (!v4l2_valid_dv_timings(&state->timings,
				   lt6911uxe_g_timings_cap(state), NULL,
				   NULL)) {
		v4l2_warn(sd, "%s: timings out of range\n", __func__);
		return -ERANGE;
	}

	*timings = state->timings;
	if (debug)
		v4l2_print_dv_timings(sd->name, "query_dv_timings: ", timings,
				      true);
	return 0;
}

static int lt6911uxe_s_stream(struct v4l2_subdev *sd, int enable)
{
	struct lt6911uxe_state *state =
		container_of(sd, struct lt6911uxe_state, sd);

	dev_dbg(sd->dev, "%s(): enable %d\n", __func__, enable);
	if (enable) {
		state->is_streaming = true;
		lt6911uxe_csi_enable(&state->sd, true);
	} else {
		state->is_streaming = false;
		lt6911uxe_csi_enable(&state->sd, false);
	}

	return 0;
}

/* ------ PAD OPS ----------------------------------------------------------- */

static int lt6911uxe_get_fmt(struct v4l2_subdev *sd,
			     struct v4l2_subdev_state *sd_state,
			     struct v4l2_subdev_format *format)
{
	struct lt6911uxe_state *state = to_state(sd);
	struct v4l2_mbus_framefmt *fmt = &format->format;
	int i = 0;

	if (format->pad != 0)
		return -EINVAL;

	/* retrieve mbus pixelcode and active video frame size */
	fmt->code = state->mbus_fmt_code;
	fmt->width = state->timings.bt.width;
	fmt->height = state->timings.bt.height;
	fmt->field = V4L2_FIELD_NONE;

	for (i = 0; i < ARRAY_SIZE(lt6911uxe_color_fmts); i++) {
		if (lt6911uxe_color_fmts[i].code == fmt->code) {
			fmt->colorspace = lt6911uxe_color_fmts[i].colorspace;
			break;
		}
	}

	switch (fmt->code) {
	case MEDIA_BUS_FMT_UYVY8_1X16:
	default:
		fmt->ycbcr_enc = V4L2_YCBCR_ENC_601;
		fmt->quantization = V4L2_QUANTIZATION_LIM_RANGE;
		break;
	}
	return 0;
}

static int lt6911uxe_set_fmt(struct v4l2_subdev *sd,
			     struct v4l2_subdev_state *sd_state,
			     struct v4l2_subdev_format *format)
{
	struct lt6911uxe_state *state = to_state(sd);
	u32 code = format->format.code; /* is overwritten by get_fmt */
	int ret;

	v4l2_dbg(2, debug, sd,
		 "%s(): query format - width=%u, height=%u, code=0x%08X\n",
		 __func__, format->format.width, format->format.height, code);

	/* adjust requested format based on current DV timings */
	ret = lt6911uxe_get_fmt(sd, sd_state, format);
	format->format.code = code;

	if (ret)
		return ret;

	switch (code) {
	case MEDIA_BUS_FMT_UYVY8_1X16:
		break;
	default:
		return -EINVAL;
	}

	if (format->which == V4L2_SUBDEV_FORMAT_TRY)
		return 0;

	state->mbus_fmt_code = format->format.code;
	v4l2_dbg(2, debug, sd,
		 "%s(): current format - width=%u, height=%u, code=0x%08X\n",
		 __func__, format->format.width, format->format.height,
		 state->mbus_fmt_code);
	return 0;
}

static int lt6911uxe_enum_mbus_code(struct v4l2_subdev *sd,
				    struct v4l2_subdev_state *sd_state,
				    struct v4l2_subdev_mbus_code_enum *code)
{
	if (code->index >= ARRAY_SIZE(lt6911uxe_color_fmts))
		return -EINVAL;

	code->code = lt6911uxe_color_fmts[code->index].code;
	v4l2_dbg(2, debug, sd, "%s(): fmt-code 0x%04X\n", __func__, code->code);

	return 0;
}

static int lt6911uxe_dv_timings_cap(struct v4l2_subdev *sd,
				    struct v4l2_dv_timings_cap *cap)
{
	struct lt6911uxe_state *state = to_state(sd);

	if (cap->pad != 0)
		return -EINVAL;

	*cap = *lt6911uxe_g_timings_cap(state);
	return 0;
}

static int lt6911uxe_enum_dv_timings(struct v4l2_subdev *sd,
				     struct v4l2_enum_dv_timings *timings)
{
	struct lt6911uxe_state *state = to_state(sd);

	if (timings->pad != 0)
		return -EINVAL;

	/* filter non supported DV timings */
	return v4l2_enum_dv_timings_cap(timings, lt6911uxe_g_timings_cap(state),
					NULL, NULL);
}

/* ------ Register OPS ------------------------------------------------------ */

static int lt6911uxe_open(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh)
{
	return 0;
}

static const struct v4l2_subdev_internal_ops lt6911uxe_subdev_internal_ops = {
	.open = lt6911uxe_open,
};

static struct v4l2_subdev_core_ops lt6911uxe_subdev_core_ops = {
	.log_status = lt6911uxe_log_status,
	.subscribe_event = lt6911uxe_subscribe_event,
	.unsubscribe_event = v4l2_event_subdev_unsubscribe,
};

static struct v4l2_subdev_video_ops lt6911uxe_subdev_video_ops = {
	.g_input_status = lt6911uxe_g_input_status,
	.s_dv_timings = lt6911uxe_s_dv_timings,
	.g_dv_timings = lt6911uxe_g_dv_timings,
	.query_dv_timings = lt6911uxe_query_dv_timings,
	.s_stream = lt6911uxe_s_stream,
};

static const struct v4l2_subdev_pad_ops lt6911uxe_pad_ops = {
	.set_fmt = lt6911uxe_set_fmt,
	.get_fmt = lt6911uxe_get_fmt,
	.enum_mbus_code = lt6911uxe_enum_mbus_code,
	.dv_timings_cap = lt6911uxe_dv_timings_cap,
	.enum_dv_timings = lt6911uxe_enum_dv_timings,
};

static struct v4l2_subdev_ops lt6911uxe_ops = {
	.core = &lt6911uxe_subdev_core_ops,
	.video = &lt6911uxe_subdev_video_ops,
	.pad = &lt6911uxe_pad_ops,
};

#ifdef CONFIG_MEDIA_CONTROLLER
static const struct media_entity_operations lt6911uxe_media_ops = {
	.link_validate = v4l2_subdev_link_validate,
};
#endif

/* ------ CUSTOM CTRLS ------------------------------------------------------ */

static const struct v4l2_ctrl_config lt6911uxe_ctrl_audio_sampling_rate = {
	.id = LT6911UXE_CID_AUDIO_SAMPLING_RATE,
	.name = "Audio Sampling Rate",
	.type = V4L2_CTRL_TYPE_INTEGER,
	.min = 0,
	.max = 192000,
	.step = 1,
	.def = 0,
	.flags = V4L2_CTRL_FLAG_READ_ONLY,
};

static const struct v4l2_ctrl_config lt6911uxe_ctrl_audio_present = {
	.id = LT6911UXE_CID_AUDIO_PRESENT,
	.name = "Audio Present",
	.type = V4L2_CTRL_TYPE_BOOLEAN,
	.min = 0,
	.max = 1,
	.step = 1,
	.def = 0,
	.flags = V4L2_CTRL_FLAG_READ_ONLY,
};

/* ------ Driver setup ------------------------------------------------------ */

static void lt6911uxe_initial_setup(struct lt6911uxe_state *state)
{
	state->mbus_fmt_code = MEDIA_BUS_FMT_UYVY8_1X16;
	state->signal_present = false;
	state->enable_i2c = false;
	mutex_init(&state->lock);

	/* Init Timings */
	lt6911uxe_s_dv_timings(&state->sd, &default_timing);
}

#ifdef CONFIG_OF

static const struct of_device_id lt6911uxe_of_match[] = {
	{.compatible = "lontium,lt6911uxe" },
	{}
};
MODULE_DEVICE_TABLE(of, lt6911uxe_of_match);

static struct lt6911uxe_platform_data *
lt6911uxe_parse_dt(struct i2c_client *client)
{
	struct device_node *node = client->dev.of_node;
	struct lt6911uxe_platform_data *pdata;
	const struct of_device_id *match;
	int gpio;

	match = of_match_device(lt6911uxe_of_match, &client->dev);
	if (!match) {
		dev_err(&client->dev,
			"Driver has not been loaded from an of_match\n");
		return ERR_PTR(-ENODEV);
	}
	pdata = devm_kzalloc(&client->dev,
			     sizeof(struct lt6911uxe_platform_data),
			     GFP_KERNEL);
	if (!pdata)
		return ERR_PTR(-ENOMEM);

	gpio = of_get_named_gpio(node, "reset-gpio", 0);
	if (gpio < 0) {
		dev_err(&client->dev, "reset-gpio read failed: (%d)\n", gpio);
		return ERR_PTR(gpio);
	}
	pdata->reset_gpio = gpio;

	return pdata;
}
#else
static struct lt6911uxe_platform_data *
lt6911uxe_parse_dt(struct i2c_client *client)
{
	return ERR_PTR(-ENODEV);
}
#endif

static int lt6911uxe_do_probe(struct i2c_client *client)
{
	struct lt6911uxe_state *state;
	struct v4l2_subdev *sd;
	int err = 0;

	dev_info(&client->dev, "Probing lt6911uxe\n");
	state = devm_kzalloc(&client->dev, sizeof(struct lt6911uxe_state),
			     GFP_KERNEL);
	if (!state)
		return -ENOMEM;

	state->pdata = lt6911uxe_parse_dt(client);

	if (IS_ERR(state->pdata))
		return PTR_ERR(state->pdata);

	state->i2c_client = client;
	sd = &state->sd;
	v4l2_i2c_subdev_init(sd, client, &lt6911uxe_ops);

	dev_info(&client->dev, "Chip found @ 7h%02X (%s)\n", client->addr,
		 client->adapter->name);

	/* initial setup */
	lt6911uxe_initial_setup(state);

	/* get interrupt */
	if (client->irq) {
		err = devm_request_threaded_irq(
			&state->i2c_client->dev, client->irq, NULL,
			lt6911uxe_irq_handler,
			IRQF_TRIGGER_FALLING | IRQF_ONESHOT, sd->name,
			(void *)sd);
		if (err) {
			dev_err(&client->dev,
				"Could not request interrupt %d! Get interrupt failed: %d\n",
				client->irq, err);
			return err;
		}
	}

	/* custom v4l2 controls */
	v4l2_ctrl_handler_init(&state->ctrl_handler, 2);
	state->audio_sampling_rate_ctrl =
		v4l2_ctrl_new_custom(&state->ctrl_handler,
				     &lt6911uxe_ctrl_audio_sampling_rate, NULL);
	state->audio_present_ctrl = v4l2_ctrl_new_custom(
		&state->ctrl_handler, &lt6911uxe_ctrl_audio_present, NULL);

	v4l2_ctrl_handler_setup(sd->ctrl_handler);
	if (state->ctrl_handler.error) {
		err = state->ctrl_handler.error;
		goto err_ctrl_handler;
	}
	sd->ctrl_handler = &state->ctrl_handler;

	/* media entitiy: define pad as output -> origins of link */
	if (IS_ENABLED(CONFIG_MEDIA_CONTROLLER)) {
		state->pad[0].flags = MEDIA_PAD_FL_SOURCE;
		sd->entity.ops = &lt6911uxe_media_ops;

		err = media_entity_pads_init(&sd->entity, 1, state->pad);
		if (err < 0) {
			dev_err(&client->dev, "unable to init media entity\n");
			goto err_ctrl_handler;
		}
	}

	/* register v4l2_subdev device */
	sd->dev = &client->dev;
	sd->internal_ops = &lt6911uxe_subdev_internal_ops;
	sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE | V4L2_SUBDEV_FL_HAS_EVENTS;

	err = v4l2_async_register_subdev(sd);
	if (err) {
		dev_err(&client->dev, "lt6911uxe subdev registration failed\n");
		goto err_ctrl_handler;
	}

	/*reset*/
	if (gpio_is_valid(state->pdata->reset_gpio)) {
		err = devm_gpio_request_one(&client->dev,
					    state->pdata->reset_gpio,
					    GPIOF_OUT_INIT_LOW,
					    "lt6911uxe_reset");
		if (err) {
			dev_err(&client->dev,
				"Failed to request reset GPIO %d\n",
				state->pdata->reset_gpio);
			goto free_ctrl;
		}
		gpio_set_value(state->pdata->reset_gpio, 0);
		msleep(10);
		gpio_set_value(state->pdata->reset_gpio, 1);
		dev_info(&client->dev, "reset GPIO %d\n",
			 state->pdata->reset_gpio);
	} else {
		dev_info(&client->dev, "reset_gpio is not valid\n");
	}

	return 0;

err_ctrl_handler:
	v4l2_ctrl_handler_free(&state->ctrl_handler);
	return err;
free_ctrl:
	v4l2_ctrl_handler_free(&state->ctrl_handler);
	v4l2_async_unregister_subdev(sd);
	return err;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(6, 5, 0)
static int lt6911uxe_probe(struct i2c_client *client)
{
	return lt6911uxe_do_probe(client);
}
#else
static int lt6911uxe_probe(struct i2c_client *client,
			   const struct i2c_device_id *id)
{
	return lt6911uxe_do_probe(client);
}
#endif

static int lt6911uxe_do_remove(struct i2c_client *client)
{
	struct v4l2_subdev *sd = i2c_get_clientdata(client);

	v4l2_async_unregister_subdev(sd);
	v4l2_ctrl_handler_free(sd->ctrl_handler);

	if (IS_ENABLED(CONFIG_MEDIA_CONTROLLER))
		media_entity_cleanup(&sd->entity);
	dev_info(&client->dev, "removed lt6911uxe instance\n");
	return 0;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(6, 1, 0)
static void lt6911uxe_remove(struct i2c_client *client)
{
	lt6911uxe_do_remove(client);
}
#else
static int lt6911uxe_remove(struct i2c_client *client)
{
	return lt6911uxe_do_remove(client);
}
#endif

static const struct i2c_device_id lt6911uxe_id[] = { { "lt6911uxe", 0 }, {} };
MODULE_DEVICE_TABLE(i2c, lt6911uxe_id);

static struct i2c_driver lt6911uxe_driver = {
	.driver = {
		.of_match_table = of_match_ptr(lt6911uxe_of_match),
		.name = "lt6911uxe",
		.owner = THIS_MODULE,
	},
	.id_table = lt6911uxe_id,
	.probe = lt6911uxe_probe,
	.remove = lt6911uxe_remove,
};
module_i2c_driver(lt6911uxe_driver);

MODULE_DESCRIPTION("Driver for Lontium lt6911uxe HDMI to CSI-2 Bridge");
MODULE_AUTHOR("Lukas Neuner <neur@zhaw.ch>");
MODULE_AUTHOR("Alexey Gromov <groo@zhaw.ch>");
MODULE_LICENSE("GPL v2");
