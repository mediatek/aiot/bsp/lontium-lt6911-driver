/* SPDX-License-Identifier: GPL-2.0 */
/*
 * lt6911uxe_regs.h - Lontium 4k60 HDMI-CSI bridge register definitions
 *
 * Copyright (c) 2020, Alexey Gromov <groo@zhaw.ch>
 * Copyright (c) 2024 MediaTek Inc.
 */

#ifndef __LT6911UXE_REGS_H__
#define __LT6911UXE_REGS_H__

/* Control */
#define SW_BANK             0xff
#define ENABLE_I2C          0xE0EE
#define MIPI_TX_CTRL        0xE0B0

/* Interrupts */
#define INT_VIDEO           0xE084
#define INT_VIDEO_DISAPPEAR 0x00
#define INT_VIDEO_READY     0x01

#define INT_AUDIO           0xE084
#define INT_AUDIO_DISAPPEAR 0x02
#define INT_AUDIO_READY     0x03

/* Timings */
#define half_PixelClock2    0xE085
#define half_PixelClock1    0xE086
#define half_PixelClock0    0xE087
#define half_Htotal1        0xE088
#define half_Htotal0        0xE089
#define Vtotal1             0xE08A
#define Vtotal0             0xE08B
#define half_Hactive1       0xE08C
#define half_Hactive0       0xE08D
#define Vactive1            0xE08E
#define Vactive0            0xE08F
#define Audio_FS_Value1     0xE090
#define Audio_FS_Value0     0xE091
#define ByteClock2          0xE092
#define ByteClock1          0xE093
#define ByteClock0          0xE094
#define MIPI_LANES          0xE095
#define MIPI_FORMAT         0xE096

#endif  /* __LT6911UXE_REGS_H__ */
